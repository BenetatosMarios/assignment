package com.example.assigment;

import android.app.Application;

import com.example.assigment.helpers.PreferenceHelper;
import com.example.assigment.net.RetroClient;
import com.example.assigment.net.RetroClientGames;
import com.example.assigment.net.RetroClientHeaders;
import com.example.assigment.net.RetroClientUpdateGames;
import com.example.assigment.net.RetroClientUpdateHeadlines;


public class NoviBet extends Application {

    private static NoviBet instance;

    public RetroClient client;
    public RetroClientGames clientGames;
    public RetroClientHeaders clientHeaders;
    public RetroClientUpdateHeadlines clientUpdateHeadlines;
    public RetroClientUpdateGames clientUpdateGames;

    public PreferenceHelper preferenceHelper;


    public static synchronized NoviBet getInstance(){
        return  instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance =this;


//        LeakCanary.install(this);
//        Realm.init(getApplicationContext());
        clientGames= new RetroClientGames(getApplicationContext());
        clientHeaders= new RetroClientHeaders(getApplicationContext());
        clientUpdateHeadlines= new RetroClientUpdateHeadlines(getApplicationContext());
        clientUpdateGames= new RetroClientUpdateGames(getApplicationContext());
        client = new RetroClient(getApplicationContext());
        preferenceHelper = new PreferenceHelper(getApplicationContext());

//        if (BuildConfig.DEBUG) {
//            Timber.plant(new Timber.DebugTree());
//        } else {
//            //no release
//        }


    }
}
