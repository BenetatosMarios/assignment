package com.example.assigment.models;

import android.app.Application;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.assigment.net.models.games.BetViews;
import com.example.assigment.net.models.games.Competitions;
import com.example.assigment.net.models.games.Events;
import com.example.assigment.net.models.headlines.HeadlinesClass;

import java.util.ArrayList;
import java.util.List;

public  class ExtraInfo extends Application implements Parcelable{

     public static List<Events> events= new ArrayList<>();
     public static List<String> competitor1Caption =new ArrayList<>();
     public static List<String> competitor2Caption=new ArrayList<>();
     public static List<String> startTime=new ArrayList<>();
     public static List<HeadlinesClass> headlinesClass;
//     public String date;
//     public String name;
//     public String phone;


    protected ExtraInfo(Parcel in) {
    }

    public static final Creator<ExtraInfo> CREATOR = new Creator<ExtraInfo>() {
        @Override
        public ExtraInfo createFromParcel(Parcel in) {
            return new ExtraInfo(in);
        }

        @Override
        public ExtraInfo[] newArray(int size) {
            return new ExtraInfo[size];
        }
    };

    public ExtraInfo() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
