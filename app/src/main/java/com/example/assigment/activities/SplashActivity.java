package com.example.assigment.activities;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.assigment.NoviBet;
import com.example.assigment.fragments.ProgressDialogFragment;
import com.example.assigment.models.ExtraInfo;
import com.example.assigment.net.models.games.BetViews;
import com.example.assigment.net.models.games.GamesResponse;
import com.example.assigment.net.models.headlines.HeadlinesClass;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private static final String PROGRESS_FRAGMENT_DIALOG_TAG = "PROGRESS_FRAGMENT_DIALOG_TAG";
    boolean fetchgamesDone = false;
    boolean fetchHeadlines = false;
    ArrayList<GamesResponse> ResponseGames = new ArrayList<>();
    ExtraInfo extraInfo= new ExtraInfo();




    public void startLoading() {
        startLoading(null);
    }

    public void startLoading(String message) {
        ProgressDialogFragment dialogFragment = (ProgressDialogFragment) getSupportFragmentManager().findFragmentByTag(PROGRESS_FRAGMENT_DIALOG_TAG);

        // instantiate our dialog fragment if needed
        // otherwise use the dialog already displayed and just change it's message
        if(dialogFragment == null) {
            dialogFragment = ProgressDialogFragment.newInstance(-1, message, false);
            dialogFragment.show(getSupportFragmentManager(), PROGRESS_FRAGMENT_DIALOG_TAG);
        } else {
            dialogFragment.setProgressMessage(message);
        }
        dialogFragment.setCancelable(false);
    }

    public  void stopLoading(int iconResId, String message) {
        ProgressDialogFragment dialogFragment = (ProgressDialogFragment) getSupportFragmentManager().findFragmentByTag(PROGRESS_FRAGMENT_DIALOG_TAG);
        if(dialogFragment != null) {
            dialogFragment.dismissWithMessage(iconResId, message);
        }
    }

    public ProgressDialogFragment showMessage(int iconResId, String message) {
//        Toast.makeText(this, message, Toast.LENGTH_LONG).show();

        ProgressDialogFragment dialogFragment = (ProgressDialogFragment) getSupportFragmentManager().findFragmentByTag(PROGRESS_FRAGMENT_DIALOG_TAG);

        // kill existing dialog and show a new one
        if(dialogFragment != null) {
            dialogFragment.dismissAllowingStateLoss();
        }

        dialogFragment = ProgressDialogFragment.newInstance(iconResId, message, true);
        dialogFragment.show(getSupportFragmentManager(), PROGRESS_FRAGMENT_DIALOG_TAG);
        dialogFragment.setCancelable(false);

        return dialogFragment;
    }

    public void stopLoading () {
        ProgressDialogFragment dialogFragment = (ProgressDialogFragment) getSupportFragmentManager().findFragmentByTag(PROGRESS_FRAGMENT_DIALOG_TAG);
        if(dialogFragment != null) {
            dialogFragment.dismissAllowingStateLoss();
        }
    }


    private void goToApp() {
        if (fetchHeadlines && fetchgamesDone) {
            Intent intent = null;
            intent = new Intent(this, MainActivity.class);
            stopLoading();
            startActivity(intent);
            finish();
        }
    }

    public void fetchGames(){
        Call<ArrayList<GamesResponse>> call = NoviBet.getInstance().clientGames.getNoviBetService().fetchGames(NoviBet.getInstance().preferenceHelper.getToken());

        call.enqueue(new Callback<ArrayList<GamesResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<GamesResponse>> call, Response<ArrayList<GamesResponse>> response) {

//

                ResponseGames = response.body();

//                ExtraInfo.events=ResponseGames.get(0).betViews.get(0).competitions.get(0);

                for (int i =0; i<ResponseGames.get(0).betViews.get(0).competitions.size();i++){
                    ExtraInfo.events.add(ResponseGames.get(0).betViews.get(0).competitions.get(i).events.get(0));
                }
                for (int i=0;i<ExtraInfo.events.size();i++){
                    String temp = ExtraInfo.events.get(i).liveData.elapsed;
                    ExtraInfo.events.get(i).liveData.elapsed = SpliteTimer(temp);
                }

                fetchgamesDone =true;
                goToApp();
            }

            @Override
            public void onFailure(Call<ArrayList<GamesResponse>> call, Throwable t) {
                Log.d(" fail " ,t.getMessage());
            }
        });
    }

//    public static <T> List<T> getTeamListFromJson(String jsonString, Type type) {
//        if (TextUtils.isEmpty(jsonString)){
//            return null;
//        }
//        return new Gson().fromJson(jsonString,type);
//    }

    public void fetchHeaders(){

        Call<ArrayList<GamesResponse>> call = NoviBet.getInstance().clientHeaders.getNoviBetService().fetchHeadlines(NoviBet.getInstance().preferenceHelper.getToken());

        call.enqueue(new Callback<ArrayList<GamesResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<GamesResponse>> call, Response<ArrayList<GamesResponse>> response) {

                ResponseGames = response.body();

                ExtraInfo.headlinesClass=new ArrayList<>();

                HeadlinesClass headlinesClass= new HeadlinesClass();

                headlinesClass.startTime = SpliteTimer(ResponseGames.get(0).betViews.get(0).startTime);
                headlinesClass.competitor1Caption =ResponseGames.get(0).betViews.get(0).competitor1Caption;
                headlinesClass.competitor2Caption= ResponseGames.get(0).betViews.get(0).competitor2Caption;

                ExtraInfo.headlinesClass.add(headlinesClass);
//                ExtraInfo.headlinesClass.startTime.add(SpliteTimer(ResponseGames.get(0).betViews.get(0).startTime));
//                ExtraInfo.headlinesClass.competitor1Caption.add(ResponseGames.get(0).betViews.get(0).competitor1Caption);
//                ExtraInfo.headlinesClass.competitor2Caption.add(ResponseGames.get(0).betViews.get(0).competitor2Caption);

                fetchHeadlines = true;
                goToApp();

            }

            @Override
            public void onFailure(Call<ArrayList<GamesResponse>> call, Throwable t) {

            }
        });
    }

    private String SpliteTimer(String startTime) {
        if (!startTime.contains("-")) {
            if (startTime.contains(".")) {
                String[] separated = startTime.split("\\.");
                String timer = separated[0];
                return timer;
            }
            return startTime;
        }
        return "0";
    }


}
