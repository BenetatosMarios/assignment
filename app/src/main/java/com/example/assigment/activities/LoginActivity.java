package com.example.assigment.activities;


import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;


import com.example.assigment.NoviBet;
import com.example.assigment.R;
import com.example.assigment.helpers.PreferenceHelper;
import com.example.assigment.net.models.LoginModel;
import com.example.assigment.net.models.LoginResp;

import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends SplashActivity {

    private static final int CONNECTION_TIMEOUT = 30;

    // UI references.
    private EditText mUsername;
    private EditText mPassword;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mUsername = findViewById(R.id.username);
//        AutoCompleteTextView autoCompleteLogin = (AutoCompleteTextView) mUsername.getEditText();


        Set<String> autocomplete = NoviBet.getInstance().preferenceHelper.getAutoCompleteUsernames();
        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, autocomplete.toArray(new String[autocomplete.size()]));
//        autoCompleteLogin.setAdapter(adapter);

        mPassword = findViewById(R.id.password);

        Button login = findViewById(R.id.submit);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkForm()) {
                    doLogin();
//                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
//                    finish();
                }
            }
        });

        // grab auto-focus from EditTexts
        findViewById(R.id.email_login_form).requestFocus();


    }

    private boolean checkForm() {
        mUsername.setError(null);
        mPassword.setError(null);

        boolean valid = true;
        if (TextUtils.isEmpty(mUsername.getText()) ) {
//                || !Patterns.EMAIL_ADDRESS.matcher(mUsername.getText()).matches()) {
            mUsername.setError(getString(R.string.error_email));
            valid = false;
        }

        if (TextUtils.isEmpty(mPassword.getText()) || mPassword.getText().length() < 4) {
            mPassword.setError(getString(R.string.error_password));
            valid = false;
        }

        return valid;
    }

    private void doLogin() {
        startLoading();
        LoginModel loginModel = new LoginModel(mUsername.getText().toString(), mPassword.getText().toString());

//        OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();
//        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
//            @Override public void log(String message) {
//
//            }
//        });
//        httpLoggingInterceptor.setLevel( HttpLoggingInterceptor.Level.BODY);
//        okClientBuilder.addInterceptor(httpLoggingInterceptor);
//
//
//        okClientBuilder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
//        okClientBuilder.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
//        okClientBuilder.writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);

        //creating retro
//        Retrofit.Builder builder = new Retrofit.Builder()
//                .baseUrl("http://www.mocky.io/v2/5d8e4bd9310000a2612b5448/")
//                .client(okClientBuilder.build())
//                .addConverterFactory(GsonConverterFactory.create());
//
//        Retrofit retrofit = builder.build();
//
//        NoviBetService client = retrofit.create(NoviBetService.class);
//        Call<LoginResp> call = client.login(loginModel);

        Call<LoginResp> call = NoviBet.getInstance().client.getNoviBetServicee().login(loginModel);

//        Call<LoginResp> call = NoviBet.getInstance().client.getAirtimeService().login(loginModel);
        call.enqueue(new Callback<LoginResp>() {
            @Override
            public void onResponse(Call<LoginResp> call, Response<LoginResp> response) {

                if (response.isSuccessful()) {
                    String username = mUsername.getText().toString();

                    PreferenceHelper preferenceHelper = NoviBet.getInstance().preferenceHelper;

                    preferenceHelper.putToken(response.body().access_token);
                    preferenceHelper.putToken_type(response.body().token_type);
                    preferenceHelper.putUsername(username);
                    preferenceHelper.putPassword(mPassword.getText().toString());
                    preferenceHelper.putAutocompleteUsername(username);
                    stopLoading();

//                    goToApp();
                    fetchGames();
                    fetchHeaders();
                } else {
                    //error na ftia3w
                    stopLoading();
                    showMessage(R.drawable.ic_dialog_error, getString(R.string.error_generic));

                }
            }



            @Override
            public void onFailure(Call<LoginResp> call, Throwable t) {

                Log.d(" fail", t.getMessage());
                stopLoading();
                showMessage(R.drawable.ic_dialog_error, getString(R.string.error_generic));

            }
        });
    }

}
