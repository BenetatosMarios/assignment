package com.example.assigment.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.widget.ListView;
import android.widget.TextView;

import com.example.assigment.NoviBet;
import com.example.assigment.R;
import com.example.assigment.adapters.AdapterGames;
import com.example.assigment.adapters.AdapterHeadlines;
import com.example.assigment.helpers.UpdateService;
import com.example.assigment.models.ExtraInfo;
import com.example.assigment.net.models.games.Events;
import com.example.assigment.net.models.games.GamesResponse;
import com.example.assigment.net.models.headlines.HeadlinesClass;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerViewHeadlines , recyclerViewGames;
    AdapterHeadlines adapterHeadlines;
    AdapterGames adapterGames;
    TextView coundown;
    CountDownTimer countDownTimer;
    long timeLeftmill= 20000;
    Boolean updateHeadlines= false , updateGames = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerViewHeadlines = findViewById(R.id.Recycle_Headlines);
        recyclerViewGames = findViewById(R.id.Recycle_Games);
        coundown = findViewById(R.id.timer);

        recyclerViewHeadlines.setHasFixedSize(true);
        recyclerViewGames.setHasFixedSize(true);

        recyclerViewHeadlines.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        recyclerViewGames.setLayoutManager(new LinearLayoutManager(this));


        adapterHeadlines= new AdapterHeadlines(this, ExtraInfo.headlinesClass);
        adapterGames= new AdapterGames(this);

        recyclerViewHeadlines.setAdapter(adapterHeadlines);
        recyclerViewGames.setAdapter(adapterGames);

        // Start service using AlarmManager
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 10);

        Intent intent = new Intent(this, UpdateService.class);

        PendingIntent pintent = PendingIntent.getService(this, 0, intent, 0);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

//        for 60 mint 60*60*1000
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                20*1000, pintent);


        startTimer();
    }

    private void startTimer() {
        countDownTimer = new CountDownTimer(timeLeftmill,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftmill=millisUntilFinished;
               updateTimer();
            }

            @Override
            public void onFinish() {
            updateItemsHolders();
            coundown.setText("20");
            countDownTimer.start();
            }
        }.start();
    }

    private void updateTimer(){
        int seconds =(int) timeLeftmill %60000 / 1000;

        String timeLeft = "";
        if (seconds<10){
            timeLeft +="0";
        }
        timeLeft += seconds;
        coundown.setText(timeLeft);
    }

    private void updateItemsHolders() {
        UpdateHeaders();
        updateGames();
    }

    private void updateGames() {
        Call<ArrayList<GamesResponse>> call = NoviBet.getInstance().clientUpdateGames.getNoviBetService().fetchGamesUpdate(NoviBet.getInstance().preferenceHelper.getToken());

        call.enqueue(new Callback<ArrayList<GamesResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<GamesResponse>> call, Response<ArrayList<GamesResponse>> response) {
                if (response.isSuccessful()){
                    ArrayList<GamesResponse> ResponseGames = new ArrayList<>();
                    ResponseGames = response.body();

                    ExtraInfo.events= new ArrayList<>();

                    for (int i=0;i< ResponseGames.get(0).betViews.get(0).competitions.get(0).events.size();i++){
//                        ExtraInfo.events.get(i).liveData.elapsed=ResponseGames.get(0).betViews.get(0).competitions.get(0).events.get(i).liveData.elapsed;
//                        ExtraInfo.events.get(i).additionalCaptions.competitor1=ResponseGames.get(0).betViews.get(0).competitions.get(0).events.get(i).additionalCaptions.competitor1;
//                        ExtraInfo.events.get(i).additionalCaptions.competitor2=ResponseGames.get(0).betViews.get(0).competitions.get(0).events.get(i).additionalCaptions.competitor2;
//
//                        Events events= new Events();
//
//                       String competitor1=ResponseGames.get(0).betViews.get(0).competitions.get(0).events.get(i).additionalCaptions.competitor1;
//                        events.additionalCaptions.competitor2=ResponseGames.get(0).betViews.get(0).competitions.get(0).events.get(i).additionalCaptions.competitor2;
//                        events.liveData.elapsed=ResponseGames.get(0).betViews.get(0).competitions.get(0).events.get(i).liveData.elapsed;

                        ExtraInfo.events.add(ResponseGames.get(0).betViews.get(0).competitions.get(0).events.get(i));
                        String temp = ExtraInfo.events.get(i).liveData.elapsed;
                        ExtraInfo.events.get(i).liveData.elapsed=SpliteTimer(temp);

                    }
                    updateGames=true;
                    gotoSychData();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GamesResponse>> call, Throwable t) {

            }
        });
    }

    private void UpdateHeaders() {
        Call<ArrayList<GamesResponse>> call = NoviBet.getInstance().clientUpdateHeadlines.getNoviBetService().fetchHeadlinesUpdate(NoviBet.getInstance().preferenceHelper.getToken());

        call.enqueue(new Callback<ArrayList<GamesResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<GamesResponse>> call, Response<ArrayList<GamesResponse>> response) {
                if (response.isSuccessful()){
                    ArrayList<GamesResponse> ResponseGames = new ArrayList<>();
                    ResponseGames = response.body();

                    ExtraInfo.headlinesClass=new ArrayList<>();

                    List<HeadlinesClass> headlinesClass=new ArrayList<>();

                    List<String> competitor1Caption =new ArrayList<>(),competitor2Caption=new ArrayList<>(),startTime=new ArrayList<>();
                    for (int i =0; i< ResponseGames.get(0).betViews.size();i++){
                        if (!TextUtils.isEmpty(ResponseGames.get(0).betViews.get(i).competitor1Caption)){
//                           ExtraInfo.competitor1Caption.add(ResponseGames.get(0).betViews.get(i).competitor1Caption);
//                           ExtraInfo.competitor2Caption.add(ResponseGames.get(0).betViews.get(i).competitor2Caption);
//                           ExtraInfo.startTime.add(ResponseGames.get(0).betViews.get(i).startTime);

                           HeadlinesClass temp = new HeadlinesClass();

                           temp.competitor1Caption=ResponseGames.get(0).betViews.get(i).competitor1Caption;
                           temp.competitor2Caption=ResponseGames.get(0).betViews.get(i).competitor2Caption;
                           temp.startTime = ResponseGames.get(0).betViews.get(i).startTime;

                           ExtraInfo.headlinesClass.add(temp);


                        }
                    }
                    updateHeadlines=true;
                    gotoSychData();
                }
            }


            @Override
            public void onFailure(Call<ArrayList<GamesResponse>> call, Throwable t) {

            }
        });
    }

    private void gotoSychData() {
        if (updateGames && updateHeadlines ){
            updateGames=false;
            updateHeadlines=false;

            AdapterHeadlines.headlinesClass=ExtraInfo.headlinesClass;
            adapterHeadlines.notifyDataSetChanged();

            AdapterGames.events=ExtraInfo.events;
            adapterGames.notifyDataSetChanged();
        }
    }

    private String SpliteTimer(String startTime) {
        if (!startTime.contains("-")) {
            if (startTime.contains(".")) {
                String[] separated = startTime.split("\\.");
                String timer = separated[0];
                return timer;
            }
            return startTime;
        }
        return "0";
    }
}
