package com.example.assigment.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.assigment.R;
import com.example.assigment.models.ExtraInfo;
import com.example.assigment.net.models.games.Events;

import java.util.ArrayList;
import java.util.List;

public class AdapterGames extends RecyclerView.Adapter<AdapterGames.GamesViewHolder>  {

    Context context;
    public static  List<Events> events = new ArrayList<>() ;

    public AdapterGames(Context context) {
        this.context = context;
        this.events = ExtraInfo.events;
    }

    @NonNull
    @Override
    public GamesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view =inflater.inflate(R.layout.item_games, null);
        GamesViewHolder gamesViewHolder = new GamesViewHolder(view);

        return gamesViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull GamesViewHolder holder, int position) {
        Events event =events.get(position);

        holder.competitor1.setText(event.additionalCaptions.competitor1);
        holder.competitor2.setText(event.additionalCaptions.competitor2);
        holder.elapsed.setText(event.liveData.elapsed);

    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    class GamesViewHolder extends RecyclerView.ViewHolder{

        TextView competitor1,competitor2,elapsed;

        public GamesViewHolder(@NonNull View itemView) {
            super(itemView);

            competitor1=itemView.findViewById(R.id.team1);
            competitor2=itemView.findViewById(R.id.team2);
            elapsed=itemView.findViewById(R.id.time);
        }
    }

}
