package com.example.assigment.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.assigment.R;
import com.example.assigment.models.ExtraInfo;
import com.example.assigment.net.models.headlines.HeadlinesClass;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class AdapterHeadlines extends RecyclerView.Adapter<AdapterHeadlines.HeadlinesViewHolder> {

    Context context;
    public static  List<HeadlinesClass> headlinesClass = new ArrayList<>() ;


    public AdapterHeadlines(Context context, List<HeadlinesClass> headlinesClass) {
        this.context = context;
        this.headlinesClass = headlinesClass;
    }

    @NonNull
    @Override
    public HeadlinesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view =inflater.inflate(R.layout.item_headlines, null);
        HeadlinesViewHolder headlinesViewHolder = new HeadlinesViewHolder(view);

        return headlinesViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HeadlinesViewHolder holder, int position) {
        HeadlinesClass headlinesClas =headlinesClass.get(position);

    holder.competitor1Caption.setText(headlinesClas.competitor1Caption);
    holder.competitor2Caption.setText(headlinesClas.competitor2Caption);
    holder.startTime.setText(headlinesClas.startTime);
    }

    @Override
    public int getItemCount() {
        return headlinesClass.size();
    }

    class HeadlinesViewHolder extends RecyclerView.ViewHolder{

        TextView competitor1Caption,competitor2Caption,startTime;

        public HeadlinesViewHolder(@NonNull View itemView) {
            super(itemView);

            competitor1Caption = itemView.findViewById(R.id.competitor1Caption);
            competitor2Caption = itemView.findViewById(R.id.competitor2Caption);
            startTime = itemView.findViewById(R.id.startTime);
        }
    }
}
