package com.example.assigment.net.models.games;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Competitions {
    @SerializedName("events")
    public  List<Events> events ;
}
