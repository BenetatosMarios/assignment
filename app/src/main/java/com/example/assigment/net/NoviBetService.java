package com.example.assigment.net;


import com.example.assigment.net.models.games.GamesResponse;
import com.example.assigment.net.models.LoginModel;
import com.example.assigment.net.models.LoginResp;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NoviBetService {

    @POST("login")
    Call<LoginResp> login(@Body LoginModel credentials);

    @Headers({"Accept: application/json"})
    @GET("games")
    Call<ArrayList<GamesResponse>> fetchGames(@Query("access_token") String access_token);

    @Headers({"Accept: application/json"})
    @GET("headlines")
    Call<ArrayList<GamesResponse>> fetchHeadlines(@Query("access_token") String access_token);

    @Headers({"Accept: application/json"})
    @GET("gamesupdate")
    Call<ArrayList<GamesResponse>> fetchGamesUpdate(@Query("access_token") String access_token);

    @Headers({"Accept: application/json"})
    @GET("headlinesupdate.")
    Call<ArrayList<GamesResponse>> fetchHeadlinesUpdate (@Query("access_token") String access_token);

}
