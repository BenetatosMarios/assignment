package com.example.assigment.net.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class LoginResp implements Parcelable {

    @SerializedName("access_token")
    public String access_token;

    @SerializedName("token_type")
    public String token_type;

    protected LoginResp(Parcel in) {
        access_token = in.readString();
        token_type = in.readString();
    }

    public static final Creator<LoginResp> CREATOR = new Creator<LoginResp>() {
        @Override
        public LoginResp createFromParcel(Parcel in) {
            return new LoginResp(in);
        }

        @Override
        public LoginResp[] newArray(int size) {
            return new LoginResp[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(access_token);
        dest.writeString(token_type);
    }
}
