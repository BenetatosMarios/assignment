package com.example.assigment.net.models.games;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GamesResponse {

    @SerializedName("betViews")
    public ArrayList<BetViews> betViews ;

    @SerializedName("hasHighlights")
    public String  hasHighlights ;

    @SerializedName("totalCount")
    public int  totalCount ;

    @SerializedName("caption")
    public String  caption ;

    @SerializedName("marketViewType")
    public String  marketViewType ;

    @SerializedName("marketViewKey")
    public String  marketViewKey ;

    @SerializedName("modelType")
    public String  modelType ;


    public GamesResponse() {

    }
}
