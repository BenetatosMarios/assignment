package com.example.assigment.net.models.games;

import com.google.gson.annotations.SerializedName;

public class AdditionalCaptions {

    @SerializedName("competitor1")
    public  String competitor1 ;

    @SerializedName("competitor2")
    public  String competitor2 ;

}
