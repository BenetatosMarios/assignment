package com.example.assigment.net.models.games;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BetViews {

    @SerializedName("competitions")
    public  ArrayList<Competitions> competitions ;

    @SerializedName("startTime")
    public  String startTime ;

    @SerializedName("competitor1Caption")
    public  String competitor1Caption ;

    @SerializedName("competitor2Caption")
    public  String competitor2Caption ;

}
