package com.example.assigment.net;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.example.assigment.NoviBet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClientUpdateGames {



        private static final int CONNECTION_TIMEOUT = 30;

        private Retrofit mRestAdapter;
        private NoviBetService mNovibetService;

        public RetroClientUpdateGames (Context context){
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            //exoun ola diaforetiko url...
            mRestAdapter = new Retrofit.Builder()
                    .baseUrl("http://www.mocky.io/v2/5d7114b2330000112177974d/")
                    .client(getOkHttpClient(context))
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

//        Retrofit.Builder builder = new Retrofit.Builder()
//                .baseUrl("http://www.mocky.io/v2/5d8e4bd9310000a2612b5448/")
//                .client(okClientBuilder.build())
//                .addConverterFactory(GsonConverterFactory.create());
        }

        public NoviBetService getNoviBetService() {
            if(mNovibetService == null) {
                mNovibetService = mRestAdapter.create(NoviBetService.class);
            }

            return mNovibetService;
        }

        private OkHttpClient getOkHttpClient(Context context){

            OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();
//        okClientBuilder.authenticator(new Authenticator() {
//            @Override public Request authenticate(Route route, Response response) throws IOException {
//                String credential = Credentials.basic(BuildConfig.BASE_URL, BuildConfig.BASE_URL);
//                return response.request().newBuilder()
//                        .header("Authorization", credential)
//                        .build();
//            }
//        });

            okClientBuilder.addInterceptor( new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request request = chain.request();

                    Headers.Builder headersBuilder = request.headers().newBuilder();

                    // Add http headers


                    // Add default query parameters
                    HttpUrl.Builder newUrlBuilder = request.url().newBuilder()
                            .addQueryParameter("response", "application/json")
                            .addQueryParameter("client", "android");

                    // if user is logged in then append his token
                    if(!TextUtils.isEmpty(NoviBet.getInstance().preferenceHelper.getToken())) {
                        newUrlBuilder.addQueryParameter("Authorization", NoviBet.getInstance().preferenceHelper.getToken_type());
                    }

                    return chain.proceed(request.newBuilder()
                            .headers(headersBuilder.build())
                            .url(newUrlBuilder.build())
                            .build()
                    );
                }
            });

            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                @Override public void log(String message) {
                    Log.d( " httpLoggingInterceptor ",message);
                }
            });
            httpLoggingInterceptor.setLevel( HttpLoggingInterceptor.Level.BODY);
            okClientBuilder.addInterceptor(httpLoggingInterceptor);

            okClientBuilder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
            okClientBuilder.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
            okClientBuilder.writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
            return okClientBuilder.build();
        }
    }



