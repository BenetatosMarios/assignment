package com.example.assigment.helpers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.assigment.NoviBet;
import com.example.assigment.net.models.games.GamesResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {

        Toast.makeText(getApplicationContext(), "Service Created", Toast.LENGTH_SHORT).show();
        super.onCreate();
    }


    public void onDestroy(AlarmManager alarm, PendingIntent pintent) {
        // TODO Auto-generated method stub
        Toast.makeText(getApplicationContext(), "Service Destroy", Toast.LENGTH_SHORT).show();
//        stopForeground(true);
        alarm.cancel(pintent);
        stopSelf();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub

//        updateHeadlines();
//        updateGames();


        Toast.makeText(getApplicationContext(), "Service Running ", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }


    private void updateHeadlines(){
        Call<ArrayList<GamesResponse>> call = NoviBet.getInstance().clientUpdateHeadlines.getNoviBetService().fetchHeadlinesUpdate(NoviBet.getInstance().preferenceHelper.getToken());

        call.enqueue(new Callback<ArrayList<GamesResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<GamesResponse>> call, Response<ArrayList<GamesResponse>> response) {
                if (response.isSuccessful()){

                    ArrayList<GamesResponse> ResponseGames = new ArrayList<>();

                    ResponseGames = response.body();

                    for (int i=0; i< ResponseGames.get(0).betViews.size();i++){

                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GamesResponse>> call, Throwable t) {

            }
        });
    }
}
