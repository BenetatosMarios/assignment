package com.example.assigment.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


public class PreferenceHelper {
    private static final String PREFERENCES_NAME = "NoviBet_preferences";

    private static final String PREF_KEY_Token_type= "token_type";
    private static final String PREF_KEY_TOKEN = "token_key";
    private static final String PREF_KEY_TOPUP_REVERSAL = "topup_reversal_key";
    private static final String PREF_KEY_STATION_DATA = "station_data_key";
    private static final String PREF_KEY_USERNAME = "username_key";
    private static final String PREF_KEY_PASSWORD = "password_key";
    private static final String PREF_KEY_DEVICE_ID = "device_id_key";
    private static final String PREF_KEY_RELEASE_CANDIDATE = "release_candidate";
    private static final String PREF_KEY_AUTOCOMPLETE_USERNAMES = "autocomplete_usernames_key";
    private static final String PREF_KEY_LOG_ENABLED = "log_enabled";

//    private static final String PREF_KEY_APP_UPDATE_AVAILABLE= "app_update_available_key";
//    private static final String PREF_KEY_RESOURCES_UPDATE_AVAILABLE= "resources_update_available_key";
//    private static final String PREF_KEY_RESOURCES_HASH= "resources_hash_key";

    private SharedPreferences mPrefs;
    private Gson mGson;

    private String mLazyToken;
    private String mLazyDeviceId;

    private String mLazyUsername;
    private String mLazyPassword;

    private String mLazyToken_type;
    private Boolean mLazyResourcesUpdateAvailable;
    private Boolean mLazyLogEnabled;

    private String mLazyResourcesHash;

    private Set<String> mLazyAutoCompleteUsernames;

//    private StationDataResponse mLazyStationData;

    public PreferenceHelper(Context context) {
        mPrefs = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);

        mGson = new GsonBuilder().create();
    }

//    public void saveStationData(StationDataResponse stationData) {
//        SharedPreferences.Editor editor = mPrefs.edit();
//        editor.putString(PREF_KEY_STATION_DATA, mGson.toJson(stationData));
//        editor.commit();
//    }

//    public StationDataResponse getStationData() {
//        try {
//            if (mLazyStationData == null) {
//                mLazyStationData = mGson.fromJson(mPrefs.getString(PREF_KEY_STATION_DATA, null), StationDataResponse.class);
//            }
//            return mLazyStationData;
//        } catch (JsonSyntaxException jse) {
//            Timber.e(jse.toString());
//            return null;
//        }
//    }
//    public void savePendingReversal(ReversibleRequest request) {
//        SharedPreferences.Editor editor = mPrefs.edit();
//        editor.putString(PREF_KEY_TOPUP_REVERSAL, mGson.toJson(request));
//        boolean result = editor.commit();
//
//        Timber.d("savePendingReversal " + request.reqNo + " " + (result ? "OK":"FAIL"));
//
//    }
//
//    public ReversibleRequest pendingReveral() {
//        try {
//            return mGson.fromJson(mPrefs.getString(PREF_KEY_TOPUP_REVERSAL, null), ReversibleRequest.class);
//        } catch (JsonSyntaxException jse) {
//            Timber.e(jse.toString());
//            return null;
//        }
//    }
//
//    public void deletePendingReversal() {
//        boolean result = mPrefs.edit().remove(PREF_KEY_TOPUP_REVERSAL).commit();
//
//        Timber.d("deletePendingReversal  " + (result ? "OK":"FAIL"));
//    }

    public void clean() {
        mLazyUsername = null;
        mLazyPassword = null;
//        mLazyStationData = null;
        mLazyToken = null;

        mPrefs.edit()
                .remove(PREF_KEY_USERNAME)
                .remove(PREF_KEY_PASSWORD)
                .remove(PREF_KEY_STATION_DATA)
                .remove(PREF_KEY_TOKEN)
                .remove(PREF_KEY_TOPUP_REVERSAL)
                .commit();
    }

    public String getDeviceId() {
        if (mLazyDeviceId == null) {
            // check if we have already generated a device id
            // otherwise generate and save a new one
            if (mPrefs.contains(PREF_KEY_DEVICE_ID)) {
                mLazyDeviceId = mPrefs.getString(PREF_KEY_DEVICE_ID, null);
            } else {
                mLazyDeviceId = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.putString(PREF_KEY_DEVICE_ID, mLazyDeviceId);
                editor.commit();
            }
        }
        return mLazyDeviceId;
    }

    public void putAutocompleteUsername(String username) {
        if(mLazyAutoCompleteUsernames == null) {
            mLazyAutoCompleteUsernames = new HashSet<>();
        }
        mLazyAutoCompleteUsernames.add(username);

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putStringSet(PREF_KEY_AUTOCOMPLETE_USERNAMES, mLazyAutoCompleteUsernames);
        editor.commit();

    }

    public Set<String> getAutoCompleteUsernames() {
        if (mLazyAutoCompleteUsernames == null || mLazyAutoCompleteUsernames.size() == 0) {
            mLazyAutoCompleteUsernames = mPrefs.getStringSet(PREF_KEY_AUTOCOMPLETE_USERNAMES, new HashSet<String>());
        }

        return mLazyAutoCompleteUsernames;
    }

    public void putToken(String token) {
        mLazyToken = token;

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(PREF_KEY_TOKEN, token);
        editor.commit();
    }

    public String getToken() {
        if(mLazyToken == null) {
            mLazyToken = mPrefs.getString(PREF_KEY_TOKEN, null);
        }
        return mLazyToken;
    }

    public void putToken_type(String token_type) {
        mLazyToken_type = token_type;

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(PREF_KEY_Token_type, token_type);
        editor.commit();
    }

    public String getToken_type() {
        if(mLazyToken_type == null) {
            mLazyToken_type = mPrefs.getString(PREF_KEY_Token_type, null);
        }
        return mLazyToken_type;
    }

    public void putUsername(String username) {
        mLazyUsername = username;

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(PREF_KEY_USERNAME, username);
        editor.commit();
    }

    public String getUsername() {
        if(mLazyUsername == null) {
            mLazyUsername = mPrefs.getString(PREF_KEY_USERNAME, null);
        }
        return mLazyUsername;
    }

    public void putPassword(String password) {
        mLazyPassword = password;

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(PREF_KEY_PASSWORD, password);
        editor.commit();
    }

    public String getPassword() {
        if(mLazyPassword == null) {
            mLazyPassword = mPrefs.getString(PREF_KEY_PASSWORD, null);
        }
        return mLazyPassword;
    }

    public void putReleaseCandidate(boolean enable) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean(PREF_KEY_RELEASE_CANDIDATE, enable);
        editor.commit();
    }

    public boolean isReleaseCandidate() {
        return mPrefs.getBoolean(PREF_KEY_RELEASE_CANDIDATE, false);
    }

    public void putLogEnabled(boolean enable) {
        mLazyLogEnabled = enable;

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean(PREF_KEY_LOG_ENABLED, enable);
        editor.commit();
    }

    public boolean isLogEnabled() {
        if (mLazyLogEnabled == null) {
            mLazyLogEnabled = mPrefs.getBoolean(PREF_KEY_LOG_ENABLED, false);
        }
        return mLazyLogEnabled;
    }

//    public void putAppUpdateAvailable(boolean enable) {
//        mLazyUpdateAvailable = enable;
//
//        SharedPreferences.Editor editor = mPrefs.edit();
//        editor.putBoolean(PREF_KEY_APP_UPDATE_AVAILABLE, enable);
//        editor.commit();
//    }
//
//    public boolean isAppUpdateAvailable() {
//        if (mLazyUpdateAvailable == null) {
//            mLazyUpdateAvailable = mPrefs.getBoolean(PREF_KEY_APP_UPDATE_AVAILABLE, false);
//        }
//        return mLazyUpdateAvailable;
//    }

//    public void putResourcesUpdateAvailable(boolean enable) {
//        mLazyResourcesUpdateAvailable = enable;
//
//        SharedPreferences.Editor editor = mPrefs.edit();
//        editor.putBoolean(PREF_KEY_RESOURCES_UPDATE_AVAILABLE, mLazyResourcesUpdateAvailable);
//        editor.commit();
//    }

//    public boolean isResourcesUpdateAvailable() {
//        if(mLazyResourcesUpdateAvailable == null) {
//            mLazyResourcesUpdateAvailable = mPrefs.getBoolean(PREF_KEY_RESOURCES_UPDATE_AVAILABLE, false);
//        }
//        return mLazyResourcesUpdateAvailable;
//    }

//    public void putResourcesHash(String hash) {
//        mLazyResourcesHash = hash;
//
//        SharedPreferences.Editor editor = mPrefs.edit();
//        editor.putString(PREF_KEY_RESOURCES_HASH, mLazyResourcesHash);
//        editor.commit();
//    }

//    public String getResourcesHash() {
//        if(mLazyResourcesHash == null) {
//            mLazyResourcesHash = mPrefs.getString(PREF_KEY_RESOURCES_HASH, BuildConfig.PRODUCTS_HASH);
//        }
//        return mLazyResourcesHash;
//    }
}
